# OEtoSTATE

OEtoSTATE.py is a python function that converts the (6) orbital elements into the cartesian dynamical state in the same inertial reference system where the elements are given (for example the mean ecpliptic and equinox of date ref system).

In the case of hyperbolic and elliptic orbits "a" is the semimajor axis, while in parabolic orbits "a" is replaced by "p" (since in that case "e=1" and "a" is infinite). Otherwise for parabolic motion, if "q" is given then "p=2q" must be passed to the OEtoState function.
